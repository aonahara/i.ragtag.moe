import { base64url } from 'rfc4648';
import GoogleDrive from './google-drive';

async function handleRequest(event: FetchEvent): Promise<Response> {
  const request = event.request;
  const url = new URL(request.url);

  if (url.pathname === '/' && request.method === 'GET') {
    return new Response(
      `<!DOCTYPE html>
<html>
  <head>
    <title>Ragtag Image</title>
    <script>
      function upload() {
        const file = document.querySelector('input').files[0]
        document.querySelector("#status").innerText = "uploading";
        fetch('/', {
          method: 'put',
          body: file
        })
        .then(res => res.json())
        .then(res => {
          document.querySelector("#status").innerText = window.location.origin + '/' + res.fileId;
        });
      }
    </script>
  </head>
  <body>
    <h1>i.ragtag.moe</h1>
    <p>encrypted file / image host</p>
    <form>
      <input type="file" />
      <button type="button" onclick="upload()">Upload</button>
      <div id="status"></div>
    </form>
    <br />
    <br />
    <h2>easier with curl</h2>
    <pre>curl --upload-file picture.jpg https://i.ragtag.moe/</pre>
  </body>
</html>
`,
      { status: 200, headers: { 'Content-Type': 'text/html' } },
    );
  } else if (request.method === 'PUT') {
    const ab = await request.arrayBuffer();

    // generate key
    const key = await crypto.subtle.generateKey(
      { name: 'AES-GCM', length: 256 },
      true,
      ['encrypt', 'decrypt'],
    );
    const keyAB = await crypto.subtle.exportKey('raw', key);

    // encrypt file
    const iv = crypto.getRandomValues(new Uint8Array(12));
    const encrypted = await crypto.subtle.encrypt(
      { name: 'AES-GCM', iv },
      key,
      ab,
    );

    // upload to drive
    const fileName = base64url.stringify(
      crypto.getRandomValues(new Uint8Array(8)),
      { pad: false },
    );
    const drive = new GoogleDrive();
    const { id } = await drive.upload(null, fileName, encrypted);

    // generate link
    const keyIv = new Uint8Array(iv.length + keyAB.byteLength);
    keyIv.set(iv, 0);
    keyIv.set(new Uint8Array(keyAB), iv.length);
    const fileId = id + base64url.stringify(keyIv, { pad: false });

    // return key to user
    return new Response(JSON.stringify({ fileId }), {
      status: 200,
      headers: { 'Content-Type': 'application/json' },
    });
  }

  try {
    const cache = caches.default;
    const cached = await cache.match(request.url);
    if (cached) return cached;

    // Get key info
    const fileId = url.pathname.substr(1);
    const driveId = fileId.substr(0, 33);
    const ivKey = base64url.parse(fileId.substr(33), { loose: true });
    const iv = new Uint8Array(12);
    iv.set(ivKey.slice(0, 12), 0);
    const key = await crypto.subtle.importKey(
      'raw',
      ivKey.slice(12),
      { name: 'AES-GCM', length: 256 },
      true,
      ['encrypt', 'decrypt'],
    );

    // Decrypt file
    const drive = new GoogleDrive();
    const file = await drive.download(driveId).then((res) => res.arrayBuffer());
    const decrypted = await crypto.subtle.decrypt(
      { name: 'AES-GCM', iv },
      key,
      file,
    );
    const resp = new Response(decrypted, {
      status: 200,
      headers: { 'Content-Length': decrypted.byteLength.toString() },
    });

    event.waitUntil(cache.put(request.url, resp.clone()));

    return resp;
  } catch (ex) {
    return new Response(JSON.stringify({ error: ex.toString() }));
  }
}

addEventListener('fetch', (event) => {
  event.respondWith(handleRequest(event));
});
