export {}

declare global {
  const K_REFRESH_TOKEN: string
  const K_DEFAULT_ROOT_ID: string
  const K_CLIENT_ID: string
  const K_CLIENT_SECRET: string

  const KV_GDRIVE_CACHE: KVNamespace
}
